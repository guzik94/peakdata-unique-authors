## How to build and run the code

Start by creating a virtualenv and activating it:

`python3 -m venv venv; . venv/bin/activate`

Then install the `pandas` library (tested on `pandas==1.3.2`):

`python3 -m pip install pandas`

Assuming you have `publications_min.csv` in the current working directory, run the script with:

`python3 solution.py`

It will write a file called `unique_authors.csv` in the current working directory.

## Documentation on your approach, i.e. what did you do and why?

Firstly, the task could be a little bit more precise. The readme states:

> Given a list of medical publications, provide a list of unique authors **and institutions**.

Later in the _Expected output_ section we can see that the output should be a two-column table with _fistname_ and _lastname_ as columns, ignoring the institutions.

Having that decision, I chose to only take care of the list of unique authors.

If we only care about the firstname and the lastname, we can ignore the middle names.

Even though there might be two distinct authors with the same firstname, lastname but with different middle names, the program would still treat them as the same author.

The solution, both on a high and low level, can differ very much. It depends on how precise do we want to be (the list of features that we want to take into account)
and what characteristics of the output we want to achieve.

One of the simpliest solution - and the one I chose to implement - is to parse and extract all authors (without middle names) from the "list of lists of authors".

Then, remove the obvious duplicates, which are the ones with the exact same firstname and lastname (ignoring case).

Then, because we treat "Antonio Abbate" and "A Abbate" as the same author, remove the duplicated authors that only have a one-letter firstname. Finally write the output
to the csv file.

## A reporting of potential failure points and bottlenecks

These are examples of possible problems with the approach and implementation:
* The program makes an assamption that for example "Antonio Abbate" and "A Abbate" are the same author, but that might not be the case.
  To be more precise, the program can take publication dates into account - if "A Abbate" happens to have published in 1948 and "Antonio Abbate" in 2021, that probably is not the same person.
* The program will produce duplicated authors which have a typo or multiple typos.

## An accounting of the remaining steps needed before putting deploying your code to a production system
Unit tests should be added. They would verify the correctness of the output based on small samples that cover the corner cases.

Typing and docstrings could be added. Refactor could be done.

Also, as stated earlier, the final solution would depend on what are the most important features of the output. One example would be to decide whether false positives are better than false negatives.

Do we care the most about not removing the potentially unique, but probably duplicated authors or do we care the most about the authors' uniqueness?

Last but not least, optimization could be done. The program runs in about 3.30 seconds on my machine.