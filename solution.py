import pandas as pd


def middle_names_removed(names):
    def _middle_name_removed(name):
        name_parts = name.split()
        return f"{name_parts[0]} {name_parts[-1]}"

    return [_middle_name_removed(name) for name in names]


def flattened_authors(data):
    authors = pd.DataFrame(data.authors.tolist(), index=data.temp_id).stack()
    authors = authors.reset_index([0, "temp_id"])
    authors.columns = ["temp_id", "authors"]

    return authors


def remove_obvious_duplicates(data, authors_lower):
    duplicates = authors_lower.duplicated()
    data.drop(data.authors[duplicates].index, inplace=True)


def transform(author):
    firstname, lastname = author.split(" ")
    initial = firstname[0]
    return (initial, lastname)


def group_by_transformed_authors(authors):
    return (
        authors.groupby("authors_transformed")["authors"]
        .apply(list)
        .reset_index(name="authors")
    )


def authors_without_unnecessary_initials(data):
    """Ignore authors that satisfy all of the following:
    - firstname is a single letter
    - there are more authors with the same first letter of the firstname and the same lastname.
    For example, it will not yield "A Lastname" if there is ["A Lastname" "Ab Lastname"] in the group."""

    def is_ignored(author, author_list):
        return author[1] == " " and len(author_list) > 1

    for author_list in data["authors"]:
        for author in author_list:
            if not is_ignored(author, author_list):
                yield author


data = pd.read_csv("publications_min.csv")
data = data.drop(
    columns=[
        "affiliations",
        "abstract",
        "title",
        "pubdate",
        "journal",
        "publication_uuid",
    ]
)
data.dropna(inplace=True)

ignored_chars = "\[|\]|'|\""
data.authors = data.authors.str.replace(ignored_chars, "", regex=True).str.split(", ")
data.authors = data.authors.map(middle_names_removed)
data = flattened_authors(data)

authors_lower = data.authors.str.lower()
remove_obvious_duplicates(data, authors_lower)

data["authors_transformed"] = authors_lower.map(transform)
data_grouped = group_by_transformed_authors(data)

data_unique = pd.DataFrame(columns=["authors"])
data_unique.authors = pd.Series(
    list(authors_without_unnecessary_initials(data_grouped))
)

data_unique[["firstname", "lastname"]] = data_unique.authors.str.split(" ", expand=True)
data_unique.drop(columns=["authors"], inplace=True)

with open("unique_authors.csv", "w+") as f:
    data_unique.to_csv(f)
